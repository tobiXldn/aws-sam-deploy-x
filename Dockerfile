FROM python:3.7

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

RUN wget --no-verbose https://github.com/aws/aws-sam-cli/releases/download/v1.27.2/aws-sam-cli-linux-x86_64.zip && \
       echo 'de615d0c4eefca60a9d7d623388767eccde93ca8c44d7f7c65e236c171cdd477 aws-sam-cli-linux-x86_64.zip' | sha256sum -c - && \
       unzip aws-sam-cli-linux-x86_64.zip -d sam-installation && ./sam-installation/install

COPY requirements.txt /usr/bin

WORKDIR /usr/bin

RUN pip install --no-cache-dir -r requirements.txt

COPY pipe.yml /usr/bin
COPY sam_pipe /usr/bin/

ENTRYPOINT ["python3", "/usr/bin/core.py"]
